package com.innominds.Junit_practice;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class AdditionOfTwoTest2 {

	AdditionOfTwo AdditionOfTwo  = new AdditionOfTwo();
    @Test
    @DisplayName("Addition using method")
    void testAdd() {
        assertEquals(11,AdditionOfTwo.add(5, 6));
    }
    @Test
    @DisplayName("Addition using Lamda expression")
    void testAddtionUsingLambda() {
        assertEquals(5, AdditionOfTwo.additionOp.operation(1, 4));
    }
}
