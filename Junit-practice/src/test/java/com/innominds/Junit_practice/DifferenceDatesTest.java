package com.innominds.Junit_practice;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class DifferenceDatesTest {

	

	@Test
	public void shouldReturnDifferenceBetweenDatesWhenDatesAreGiven() {
		
		DifferenceDates DifferenceDates = new DifferenceDates();
		LocalDate localDate1 = LocalDate.of(2022 ,9,12);
		LocalDate localDate2 = LocalDate.of(2021, 7, 9);
		LocalDate ExpectedLocalDate = LocalDate.of(1,2,3);
		LocalDate ActualLocalDate = DifferenceDates.differenceBetweenDates(localDate1, localDate2);
		Assertions.assertEquals(ExpectedLocalDate, ActualLocalDate);
	}

}