package com.innominds.Junit_practice;
import static org.junit.jupiter.api.Assertions.*;



import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class duplicateCharTest {

	  @Test
	    void testforOneduplicate() {
	        String input="hello";
	        int result=duplicateChar.countofDuplicates(input);
	        assertNotNull(input);
	        assertEquals(1,result);
	    }
	    
	    @Test
	    void testforTwoduplicate() {
	        String input="helllo";
	        int result=duplicateChar.countofDuplicates(input);
	        assertNotNull(input);
	        assertEquals(2,result);
	    }
	    
	    @Test
	    void testforThree() {
	        String input="hellloo";
	        int result=duplicateChar.countofDuplicates(input);
	        assertNotNull(input);
	        assertEquals(3,result);
	    }
	  

}
