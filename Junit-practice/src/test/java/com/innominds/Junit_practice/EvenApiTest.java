package com.innominds.Junit_practice;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


import java.util.Arrays;
import java.util.List;


class EvenApiTest {

	@Test
	public void shouldReturnEventNumberWhenArrayOfNumberIsGiven() {
		EvenApi main = new EvenApi();
		List<Integer> arrayList = Arrays.asList(1,2,3,4,5,6,7,8,9,10);
		List<Integer> expectedEvenNumbers = Arrays.asList(2,4,6,8,10);
		List<Integer> actualEvenNumbers = main.getEvenNumbers(arrayList);
		Assertions.assertEquals(expectedEvenNumbers,actualEvenNumbers);
	
	}
}
