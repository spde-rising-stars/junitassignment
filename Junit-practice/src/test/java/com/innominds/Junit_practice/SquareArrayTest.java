package com.innominds.Junit_practice;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

class SquareArrayTest {

	@Test
	void testSquares() {
		SquareArray sa = new SquareArray();
		List<Integer> input= Arrays.asList(1,2,3,4,5,6,7,8,9,10);
		List<Integer> expected= Arrays.asList(1, 4, 9, 16, 25, 36, 49, 64, 81, 100);
		assertArrayEquals(expected.toArray(),sa.squares(input).toArray());
		
		
	}

}
