package com.innominds.Junit_practice;

import static org.junit.jupiter.api.Assertions.*;

import static org.junit.jupiter.api.Assertions.assertNull;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;



import org.junit.jupiter.api.Test;


class Assignment6Test {
	 Assignment6 ass =new Assignment6 ();
	@Test
	void testSaveEvenNumbers() {
        List<Integer> expectedList = Arrays.asList(2, 4, 6, 8, 10);
        assertArrayEquals(expectedList.toArray(), ass.saveEvenNumbers(10).toArray());
	}
	void testPrintEvenNumbers() {
        ArrayList<Integer> inputList = ass.saveEvenNumbers(10);
        List<Integer> resultList = Arrays.asList(4, 8, 12, 16, 20);
        assertArrayEquals(resultList.toArray(), ass.printEvenNumbers(inputList).toArray());
    }

}
