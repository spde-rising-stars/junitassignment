package com.innominds.Junit_practice;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class OccuranceOfCharTest {

	@Test
	void test() {
		int count = OccuranceOfChar.prCharWithFreq("elephant", "e");
		assertEquals(2, count);
	}

}
