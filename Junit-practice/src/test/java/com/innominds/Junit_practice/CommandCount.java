package com.innominds.Junit_practice;

public class CommandCount {

	static // Java program to count total
	// number of words in the string
	
	 int countWords(String str)
	 {
	  
	  
	  if (str == null || str.isEmpty())
	   return 0;
	  
	 
	  String[] words = str.split("\\s+");
	  
	  
	  return words.length;
	 }
	 
	 // Driver Code
	 public static void main(String args[])
	 {
	 
	  String str =
	  "One two  three\n four\tfive ";
	  
	  // Print the result
	  System.out.println("No of words : " +
	  countWords(str));
	 }
	}
	


