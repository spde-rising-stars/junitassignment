package com.innominds.Junit_practice;

public class AdditionOfTwo {
	public int add(int a,int b) {
        return a+b;
        
    }



   @FunctionalInterface
    interface MathOperation {
        int operation(int a, int b);
    }
    
    MathOperation additionOp = (a, b) -> a + b;

}
