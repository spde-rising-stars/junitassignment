package com.innominds.Junit_practice;

import java.time.LocalDate;

public class DifferenceDates {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	public LocalDate differenceBetweenDates( LocalDate localDate1,LocalDate localDate2) {
		int Year1 = localDate1.getYear();
		int Monthvalue1 = localDate1.getMonthValue();
		int DayOfMonth1 = localDate1.getDayOfMonth();
		int Year2 = localDate2.getYear();
		int MonthValue2= localDate2.getMonthValue();
		int DayOfMonth2 = localDate2.getDayOfMonth();
		return LocalDate.of( Year1-Year2,  Monthvalue1-MonthValue2, DayOfMonth1-DayOfMonth2);
	}
	
}
